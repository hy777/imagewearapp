package jp.ne.goo.bsearch.imagesearchwearapp;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataItemBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Listens to DataItems and Messages from the local node.
 */
public class WearListenerService extends WearableListenerService {

    private static final String TAG = "WearListenerService";

    private static final String START_ACTIVITY_PATH = "/start-activity";
    private static final String DATA_ITEM_RECEIVED_PATH = "/data-item-received";
    public static final String COUNT_PATH = "/count";
    public static final String IMAGE_PATH = "/image";
    public static final String IMAGE_KEY = "photo";
    private static final String COUNT_KEY = "count";
    private static final int MAX_LOG_TAG_LENGTH = 23;
    private static final int TIMEOUT_MS = 100;
    GoogleApiClient mGoogleApiClient;

    @Override
    public void onCreate() {
        Log.v(TAG, "onCreate");
        super.onCreate();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.v(TAG, "onDataChanged: " + dataEvents);

        for (DataEvent event : dataEvents) {
            Log.v(TAG, "dataEvent:" + event);
            DataItem dataItem = event.getDataItem();

            //Message受信時
            if (COUNT_PATH.equals(dataItem.getUri().getPath())) {
                Log.v(TAG, "in count path process");
                DataMap dataMap = DataMapItem.fromDataItem(dataItem).getDataMap();
                int count = dataMap.getInt(COUNT_KEY);
                Log.v(TAG, "count:" + count);
            }

            //画像ファイル受信時
            if (IMAGE_PATH.equals(dataItem.getUri().getPath())) {
                Log.v(TAG, "in image process");
                Log.v(TAG, "Image receive start");
                DataMapItem dataMapItem = DataMapItem.fromDataItem(event.getDataItem());
                Asset profileAsset = dataMapItem.getDataMap().getAsset(IMAGE_KEY);
                Bitmap bitmap = loadBitmapFromAsset(profileAsset);
                Log.v(TAG, "Image Height : " + bitmap.getHeight());
                Log.v(TAG, "Image Width : " + bitmap.getWidth());
                Log.v(TAG, "Image receive end");

                Log.v(TAG, "intent start");
                Intent intent = new Intent(this, SearchResultActivity.class);
                intent.putExtra("resultImg", bitmap);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }

    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.v(TAG, "onMessageReceived: " + messageEvent);
        super.onMessageReceived(messageEvent);
    }

    @Override
    public void onPeerConnected(Node peer) {
        Log.v(TAG, "onPeerConnected: " + peer);
    }

    @Override
    public void onPeerDisconnected(Node peer) {
        Log.v(TAG, "onPeerDisconnected: " + peer);
    }

    public Bitmap loadBitmapFromAsset(Asset asset) {
        if (asset == null) {
            throw new IllegalArgumentException("Asset must be non-null");
        }
        ConnectionResult result =
                mGoogleApiClient.blockingConnect(TIMEOUT_MS, TimeUnit.MILLISECONDS);
        if (!result.isSuccess()) {
            return null;
        }
        // convert asset into a file descriptor and block until it's ready
        InputStream assetInputStream = Wearable.DataApi.getFdForAsset(
                mGoogleApiClient, asset).await().getInputStream();

        //mGoogleApiClient.disconnect(); //間違っているらしい．http://lastshooting.blogspot.jp/2014/11/android-wear.html

        if (assetInputStream == null) {
            Log.w(TAG, "Requested an unknown Asset.");
            return null;
        }
        // decode the stream into a bitmap
        return BitmapFactory.decodeStream(assetInputStream);
    }

}


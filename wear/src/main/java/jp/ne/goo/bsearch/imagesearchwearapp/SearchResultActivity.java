package jp.ne.goo.bsearch.imagesearchwearapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

public class SearchResultActivity extends Activity {

    private static final String TAG = "SearchResultActivity";

    private TextView mTextView;
    private ImageView mImageView;
    private Bitmap mBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        mBitmap = (Bitmap) b.get("resultImg");
        Log.v(TAG, "intent receive image height :" + mBitmap.getHeight());
        Log.v(TAG, "intent receive image width :" + mBitmap.getWidth());


        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mImageView = (ImageView)stub.findViewById(R.id.image_result);
                mImageView.setImageBitmap(mBitmap);
            }
        });
    }
}

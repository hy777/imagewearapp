package jp.ne.goo.bsearch.imagesearchwearapp;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataItemBuffer;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Listens to DataItems and Messages from the local node.
 */
public class MobileListenerService extends WearableListenerService {

    private static final String TAG = "MobileListenerService";

    private static final String START_ACTIVITY_PATH = "/start-activity";
    private static final String DATA_ITEM_RECEIVED_PATH = "/data-item-received";
    public static final String COUNT_PATH = "/count";
    public static final String IMAGE_PATH = "/image";
    public static final String IMAGE_KEY = "photo";
    private static final String COUNT_KEY = "count";
    private static final int MAX_LOG_TAG_LENGTH = 23;
    GoogleApiClient mGoogleApiClient;
    private int count = 0;
    //private String[] rqURL;
    private String rqURL;
    private Bitmap mBitmap;
    private String searchStr;

    @Override
    public void onCreate() {
        super.onCreate();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        super.onDataChanged(dataEvents);
        Log.v(TAG, "onDataChanged" );
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.v(TAG, "onMessageReceived: " + messageEvent);
        super.onMessageReceived(messageEvent);

        // Check to see if the message is to start an activity
        if (messageEvent.getPath().equals(START_ACTIVITY_PATH)) {
            if (messageEvent.getData() != null) {
                String text = null;
                try {
                    //text = new String(messageEvent.getData(), "UTF-8");
                    searchStr = new String(messageEvent.getData(), "UTF-8");
                    Log.v(TAG, "Received String is : " + searchStr);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }


            //画像取得（本来はjson取得 -> Parse -> 画像取得をキャッシュを使って実施する）
//            ++count;
//            int index = count%4;
//            rqURL = new String[10];
//            rqURL[0] = "http://dcm-cache.bsearch.goo.ne.jp/t/i?q=99&sx=180&sy=180&url=/16/a6/48/56c4cac86fea0d308545c1f6c7dc44e6391c333a95a6148a2f4486d69e3e_0.jpg&sv=0016&rx=0.408333&ry=0.165625&rw=0.320833&rh=0.240625";
//            rqURL[1] = "http://dcm-cache.bsearch.goo.ne.jp/t/i?q=99&sx=180&sy=180&url=/19/a5/a5/26c258e28fabc8d0eca09ae590cfc58c369159cdbd6d90b7e0a89aa9afcc_0.jpg&sv=0019";
//            rqURL[2] = "http://dcm-cache.bsearch.goo.ne.jp/t/i?q=99&sx=180&sy=180&url=/14/f9/4b/b59575cc6a302e76bde83c2a11506454ec3405742be24ea7f15f0429db27_0.jpg&sv=0014&rx=0.215596&ry=0.233333&rw=0.495413&rh=0.36";
//            rqURL[3] = "http://dcm-cache.bsearch.goo.ne.jp/t/i?q=99&sx=180&sy=180&url=/20/22/1b/facf03add02124df02a2420e2eda446372d4b2097accbcf9bd070b2bb657_0.jpg&sv=0020";

            if(searchStr.equals("綾瀬はるか")){
                rqURL = "http://dcm-cache.bsearch.goo.ne.jp/t/i?q=99&sx=180&sy=180&url=/14/f9/4b/b59575cc6a302e76bde83c2a11506454ec3405742be24ea7f15f0429db27_0.jpg&sv=0014&rx=0.215596&ry=0.233333&rw=0.495413&rh=0.36";
            }else if(searchStr.equals("東京タワー")){
                rqURL = "http://dcm-cache.bsearch.goo.ne.jp/t/i?q=99&sx=180&sy=180&url=/4/82/31/5950bf33be37dd8636affbe239c03da5d74c141487e7e03cbfa400356d17_0.jpg&sv=0004";
            }else if(searchStr.equals("スカイツリー")) {
                rqURL = "http://dcm-cache.bsearch.goo.ne.jp/t/i?q=99&sx=180&sy=180&url=/10/ca/a0/514cc5568d894250c02db629c5c818baedad39ab20fbbe41137fc003c1ab_0.jpg&sv=0010";
            }else if(searchStr.equals("ラーメン")) {
                rqURL = "http://dcm-cache.bsearch.goo.ne.jp/t/i?q=99&sx=180&sy=180&url=/11/28/a9/b8f40736c0402e1c5d85668ba10022f76d48d21cca8e36589b7394905a82_0.jpg&sv=0011";
            }else{
                rqURL = "http://dcm-cache.bsearch.goo.ne.jp/t/i?q=99&sx=180&sy=180&url=/3/75/1d/a34fde003a4869f26c791d5be3bffc7e2a704f14a563fa19622493d5a25f_0.jpg&sv=0003";
            }

            try {
                Log.v(TAG, "Get image start");
                InputStream input = new URL(rqURL).openStream();
                mBitmap = BitmapFactory.decodeStream(input);
                //Log.v(TAG, "image index" + index);
                Log.v(TAG, "bitmap height" + mBitmap.getHeight());
                Log.v(TAG, "bitmap width" + mBitmap.getWidth());
                Log.v(TAG, "Get image end");
            }catch (Exception e){
                Log.e(TAG, "GET IMAGE ERROR : " + e);
            }

            //画像の共有
            Log.v(TAG, "Image sync start");
            Asset asset = createAssetFromBitmap(mBitmap);
            PutDataMapRequest dataMap = PutDataMapRequest.create(IMAGE_PATH);
            dataMap.getDataMap().putAsset(IMAGE_KEY, asset);
            PutDataRequest request = dataMap.asPutDataRequest();
            PendingResult<DataApi.DataItemResult> pendingResult = Wearable.DataApi
                    .putDataItem(mGoogleApiClient, request);
            pendingResult.setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                @Override
                public void onResult(DataApi.DataItemResult dataItemResult) {
                    Log.d(TAG, "Image sync end");
                }
            });
        }
    }

    @Override
    public void onPeerConnected(Node peer) {
        Log.v(TAG, "onPeerConnected: " + peer);
    }

    @Override
    public void onPeerDisconnected(Node peer) {
        Log.v(TAG, "onPeerDisconnected: " + peer);
    }


    private static Asset createAssetFromBitmap(Bitmap bitmap) {
        final ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteStream);
        return Asset.createFromBytes(byteStream.toByteArray());
    }

    private static Asset toAsset(Bitmap bitmap) {
        ByteArrayOutputStream byteStream = null;
        try {
            byteStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteStream);
            return Asset.createFromBytes(byteStream.toByteArray());
        } finally {
            if (null != byteStream) {
                try {
                    byteStream.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
    }

}

